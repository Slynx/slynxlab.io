---
layout: post
title: "Rozszerzona struktura pracownicza"
date: 2019-10-13 21:00:00
tags: Enova Slynx.Enova.StrukturaExtended
description: Dodatek przedstawiający strukturę organizacyjną w czytelniejszej formie...
---

Rozszerzona struktura pracownicza to drobne rozszerzenie do [Enovy](https://www.enova.pl/), które wykorzystując standardową strukturę organizacyjną buduje ją w bardziej czytelnej formie.
Weźmy za przykład prostą strukturę z trzema kierownikami i trzema pracownikami.
Poniższy screen przedstawia standardowy wygląd struktury w enovie.

![img]({{ '/assets/posts/2019-10-15/str_base.png' | relative_url }}){: .center-image }
Prawa strona przedstawia listę pracowników+kierowników.
Pojęcie 'struktura' traci tutaj znaczenie, bo lista nie prezentuje podległości.
Lewa strona pokazuje strukturę w bardziej przystępny sposób, ale nie można na jej podstawie określić kto jest pracownikiem, a kto kierownikiem. 
Wymagane jest sprawdzenie elementu lub dodatkowe oznaczenie, np. w formie prefixu.
Widok tej samej struktury prezentowany przez dodatek przedstawia się następująco

![img]({{ '/assets/posts/2019-10-15/str_extended.png' | relative_url }}){: .center-image }

Zaraz nad listą są dodatkowe dwa filtry, które ograniczają dane tylko do wybranego pracownika/kierownika.
Na samej górze można zobaczyć trzy dodatkow przyciski:
- Przypisz kierownika...
- Przypisz pracownika...
- Usuń

Widoczność tych elementów jest zależna od zaznaczonego wiersza, np. nie można dodać kierownika pod pracownikiem, ale można kierownika pod kierownikiem (kierownik wyższego poziomu).
Dodatkowo struktura jest cache'owa w momencie otwarcia listy i aktualizowana na żądanie/po zmianie, więc działanie jest znacznie szybsze co jest zauważalne zwłaszcza w przypadku dużych struktur.

Poniżej krótkie wideo prezentujące podstawowe funkcje, konfigurację i sposób dodawania/usuwania pracownika/kierownika.
<video src="{{ '/assets/posts/2019-10-15/video.mp4' | relative_url }}" media="all and (max-width:480px)" controls style="width:100%" preload></video>