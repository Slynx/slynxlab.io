---
layout: post
title: "PowerShell - Zmiana hasła operatora enova"
date: 2019-10-17 11:00:00
tags: Enova PowerShell
description: Jak zmienić hasło operatora enova bez dostępu do... enovy
---

Czas na rozwiązanie niekończącego się problemu - 'Ktoś pamięta hasło, prawda?😅'

A tak na poważnie.
Niejdnokrotnie zdarza się, że mamy do czynienia z bazą do której nie możemy się zalogować, bo (jeszcze) nie mamy hasła lub ktoś tak zmienił hasło, że zanim je usłyszeliśmy wszyscy je zapomnieli😒
  
Od dawna dla każdej bazy (testowej, oczywiście😂) na wstępie resetuje hasło.
Sposobów na usunięcie hasła administratora jest kilka, ale chyba najbardziej znana/popularna metoda to prosta aplikacja, która wywołuje wewnętrzną metodę `ResetPassword` na operatorze.
Wszystko wydaje się proste, ale gdy przychodzi do praktyki:
- Trzeba załadować odpowiednie biblioteki jeśli baza z nich korzysta
- Trzeba przekazać listę baz danych przez parametr
- Trzeba pamiętać o wersji enovy
- To aplikacja. Ma swój projekt i solucje... git, wersjonowanie, kontrola... testy? (hell no😑)
- ... samo uruchamianie enovy to już duży wysiłek, nie dałoby się tak bez...😒

**PowerShell** - brak enovy, brak projektu, brak kompilacji - jeden (skrypto)plik.

{% highlight PowerShell %}
#
# Created by Slynx on 16/10/2019 22:01.
# Last modified on 16/10/2019 22:01.
#
Function Reset-Enova-Password
{
    param([hashtable]$pars, [string]$operator, [string]$newPassword)

    [byte[]] $key = 255, 255, ...

    $dbGuid = [Guid]::Parse((Invoke-Sqlcmd -Query "SELECT Value from SystemInfos where Ident = '7'" @pars)['Value']).ToByteArray()
    $passBytes = [System.Text.Encoding]::Unicode.GetBytes($newPassword)
    $opGuid = [Guid]::Parse((Invoke-Sqlcmd -Query "SELECT Guid from Operators where Name = '$operator'" @pars)['Guid']).ToByteArray()
    $totalBytes = $dbGuid + $passBytes + $opGuid
    $alg = New-Object System.Security.Cryptography.MACTripleDES -ArgumentList @(, $key)
    
    $newHash = ($alg.ComputeHash($totalBytes) | ForEach-Object ToString X2) -join ''
    Invoke-Sqlcmd -Query "UPDATE [Operators] SET [Password] = '$newHash' WHERE [Name] = '$operator'" @pars
}

$pars = @{}
$pars['ServerInstance'] = $(Read-Host "Server instance (Enter for default '.\')")
if ([string]::IsNullOrWhiteSpace($pars['ServerInstance'])) 
{ 
    $pars['ServerInstance'] = '.\'
} 

$pars['Database'] = $(Read-Host "Database name")

$op = $(Read-Host "Operator name (Enter for default 'Administrator')")
if ([string]::IsNullOrWhiteSpace($op))
{
    $op = 'Administrator'
}

$newPass = $(Read-Host "New password")
$user = $(Read-Host "SQL Username (Enter for integrated login)")
$pass = $(Read-Host "SQL Password (Enter for integrated login)")
If (-not [string]::IsNullOrWhiteSpace($user)) 
{ 
 $pars['Username'] = $user
 $pars['Password'] = $pass
}

Write-Host (Reset-Enova-Password $pars $op $newPass)
Write-Host "Password changed for $op to '$newPass'"
{% endhighlight %}

Opisywać skryptu nie ma sensu, jest dość prosty.  
Istotna jest tylko funkcja `Reset-Enova-Password` który zmienia hasło, reszta to tylko interfejs zbierający dane z konsoli.
Warto zauważyć, że resetowanie hasła odbywa się w jednej funkcji, która wymaga kilka parametrów.  
Taką funkcję można w prosty sposób wywołać w innym skrypcie... w kilka sekund można wyczyścić/zmienić hasła dla setek operatorów z wielu baz... całkiem niebezpieczne😨  
  
Skrypt po uruchomieniu potrzebuje kilku wartości  

![img]({{ '/assets/posts/2019-10-16/enova-reset-pass.png' | relative_url }}){: .center-image }

- Nazwa instancji - na serwerze developerskim często jest to domyślna instancja, więc `Enter`{: .key}
- Nazwa bazy (SQL) - tutaj nie obejdzie się bez wpisania, skrypt tego nie zgadnie
- Nazwa operatora - Domyślnie zmieniamy Administratora, więc `Enter`{: .key}
- Nowe hasło - Można wpisać, albo ustawić na puste...`Enter`{: .key}
- SQL Login - Jeśli logowanie zintegrowane to `Enter`{: .key}
- SQL Hasło - Jeśli logowanie zintegrowane to `Enter`{: .key}

Wprawne oko wypatrzy w skrypcie jeden brak, zmienna `$key = 255, 255, ...`  
Oczywiście nie jest to prawidłowa wartość. W tym miejscu niezbędny jest klucz szyfrowania operatora - ciąg kilkunastu bajtów potrzebnych do hashowania hasła.
Klucz jest własnością Sonety i nie mogę go upublicznić, ale jak wspomniałem, skrypt jest do celów developerskich, więc nie powinno być problemem znalezienie go na własne potrzeby😏

Skrypt jest dostępny na [GitHub](https://gist.github.com/MrSlynx/20a52e61079139f41a4eb4341c9cb0c2) (tylko poprzez link)